//
//  RestaurantPlainObject.swift
//  Yandex_test
//
//  Created by Nikita on 20/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

struct RestaurantPlainObject: Decodable {
    let id: Int
    let name: String
    let description: String
    let picture: RestaurantPicturePlainObject
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decode(Int.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.description = try container.decode(String.self, forKey: .description)
        self.picture = try container.decode(RestaurantPicturePlainObject.self, forKey: .picture)
    }
}

extension RestaurantPlainObject {
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description = "deliveryConditions"
        case picture
    }
}
