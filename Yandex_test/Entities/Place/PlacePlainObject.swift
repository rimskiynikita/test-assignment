//
//  PlacePlainObject.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

struct PlacePlainObject: Decodable {
    let restaurant: RestaurantPlainObject
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.restaurant = try container.decode(RestaurantPlainObject.self, forKey: .restaurant)
    }
}

extension PlacePlainObject {
    enum CodingKeys: String, CodingKey {
        case restaurant = "place"
    }
}
