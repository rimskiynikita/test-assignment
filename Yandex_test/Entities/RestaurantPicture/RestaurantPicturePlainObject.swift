//
//  RestaurantPicturePlainObject.swift
//  Yandex_test
//
//  Created by Nikita on 20/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

struct RestaurantPicturePlainObject: Decodable {
    let uri: String
    let ratio: Double
    
    func url(width: Int, height: Int) -> String {
        return (AppConstants.baseURL + uri)
            .replacingOccurrences(of: "{w}", with: "\(width)")
            .replacingOccurrences(of: "{h}", with: "\(height)")
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.uri = try container.decode(String.self, forKey: .uri)
        self.ratio = try container.decode(Double.self, forKey: .ratio)
    }
}

extension RestaurantPicturePlainObject {
    enum CodingKeys: String, CodingKey {
        case uri
        case ratio
    }
}
