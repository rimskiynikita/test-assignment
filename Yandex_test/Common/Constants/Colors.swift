//
//  Colors.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

struct Colors {
    static var blue = UIColor(hexString: "23A4FA")
    static var lightGray = UIColor(hexString: "C5C5C5")
    static var gray = UIColor(hexString: "4F4F4F")
}
