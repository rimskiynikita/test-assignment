//
//  UICollectionViewExtensions.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UICollectionView {
    func register<T>(_: T.Type) where T: UICollectionViewCell {
        register(T.self, forCellWithReuseIdentifier: T.reuseIdentifier)
    }
    
    func dequeuReusableCell<T: ReusableView>(for indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(withReuseIdentifier:
            T.reuseIdentifier, for: indexPath) as! T
    }
}

