//
//  UIStoryboardExtensions.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func instantiateViewController<T: ReusableView>() -> T {
        return instantiateViewController(withIdentifier: T.reuseIdentifier) as! T
    }
}
