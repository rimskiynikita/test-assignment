//
//  FadeAnimator.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class FadeAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    let presenting: Bool
    let duration: TimeInterval
    
    init(presenting: Bool, duration: TimeInterval = 0.25) {
        self.presenting = presenting
        self.duration = duration
        
        super.init()
    }
    
    func transitionDuration(using transitionContext:
        UIViewControllerContextTransitioning?) -> TimeInterval {
        return self.duration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let toVC = transitionContext.viewController(forKey: .to),
            let fromVC = transitionContext.viewController(forKey: .from) else { return }
        
        if presenting {
            transitionContext.containerView.addSubview(toVC.view)
            toVC.view.alpha = 0
        }
        
        UIView.animate(withDuration: self.duration, delay: 0, options:
            .transitionCrossDissolve, animations: {
                if self.presenting {
                    toVC.view.alpha = 1
                } else {
                    fromVC.view.alpha = 0
                }
        }) { _ in
            transitionContext.completeTransition(
                !transitionContext.transitionWasCancelled)
        }
    }
}

