//
//  RestaurantsPresenter.swift
//  Yandex_test
//
//  Created by Nikita on 20/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import CoreLocation

class RestaurantsListPresenter: RestaurantsListViewOutput {
    weak var view: RestaurantsListViewInput?
    var router: RestaurantsListRouterInput?
    var interactor: RestaurantsListInteractorInput?
    
    var lastCoordinate: CLLocationCoordinate2D? {
        didSet {
            if let coordinate = lastCoordinate {
                interactor?.fetchRestaurants(latitude:
                    coordinate.latitude, longitude: coordinate.longitude)
                interactor?.reverseGeocodeLocation(coordinate)
            }
        }
    }
    func didTriggerViewDidAppear() {
        interactor?.startObservingUserLocation()
    }
    
    func didTriggerViewWillDisappear() {
        interactor?.stopObservingUserLocation()
    }
    
    func didTapSettingsButton() {
        router?.openSettingsModule()
    }
    
    func didTapRetry() {
        if let coordinate = lastCoordinate {
            interactor?.fetchRestaurants(latitude:
                coordinate.latitude, longitude: coordinate.longitude)
            interactor?.reverseGeocodeLocation(coordinate)
        }
    }
}

extension RestaurantsListPresenter: RestaurantsListInteractorOutput {
    func didReceiveRestaurants(_ restaurants: [RestaurantPlainObject]) {
        let cellObjectBuilder = RestaurantCellObjectBuilder()
        let cellObjects = cellObjectBuilder.cellObjects(for: restaurants)
        view?.configure(with: cellObjects)
    }
    
    func didReceiveError() {
        view?.showError()
    }
    
    func userLocationDidChange(_ coordinate: CLLocationCoordinate2D?) {
        
        // Avoiding too frequent requests
        if let coordinate = coordinate, let lastCoordinate = lastCoordinate,
            lastCoordinate.distance(from: coordinate) < 500 {
            return
        }
        lastCoordinate = coordinate
    }
    
    func didReceiveGeocodePlacemark(_ placemark: CLPlacemark) {
        if let street = placemark.name, let city = placemark.locality {
            let address = "\(street), \(city)"
            view?.setCurrentLocationTitle(address)
        }
    }
}
