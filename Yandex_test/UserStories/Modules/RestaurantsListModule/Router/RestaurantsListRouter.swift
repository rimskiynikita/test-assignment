//
//  RestaurantsListRouter.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class RestaurantsListRouter: NSObject, RestaurantsListRouterInput {
    func openSettingsModule() {
        let settingsModuleBuilder = SettingsModuleBuilder()
        let settingsModule = settingsModuleBuilder.create()
        
        settingsModule.transitioningDelegate = self
        settingsModule.modalPresentationStyle = .custom
        
        topViewController?.present(settingsModule, animated: true, completion: nil)
    }
}

extension RestaurantsListRouter: UIViewControllerTransitioningDelegate {
    func animationController(forPresented presented:
        UIViewController, presenting: UIViewController, source:
        UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return FadeAnimator(presenting: true)
    }
    
    func animationController(forDismissed dismissed:
        UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return FadeAnimator(presenting: false)
    }
}
