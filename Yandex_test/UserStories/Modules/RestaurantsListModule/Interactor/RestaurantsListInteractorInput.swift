//
//  RestaurantsListInteractorInput.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import CoreLocation

protocol RestaurantsListInteractorInput: class {
    func fetchRestaurants(latitude: Double, longitude: Double)
    
    func startObservingUserLocation()
    func stopObservingUserLocation()
    
    func reverseGeocodeLocation(_ coordinate: CLLocationCoordinate2D)
}
