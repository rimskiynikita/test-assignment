//
//  RestaurantsListInteractor.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import CoreLocation

class RestaurantsListInteractor: RestaurantsListInteractorInput {
    weak var output: RestaurantsListInteractorOutput?
    
    var networkService: NetworkServiceProtocol?
    var userLocationService: UserLocationServiceInput?

    func fetchRestaurants(latitude: Double, longitude: Double) {
        let target = RestaurantsAPI.catalogue(latitude: latitude, longitude: longitude)
        networkService?.fetchObjects(target, keyPath: "payload.foundPlaces") {
            [weak self] (response: NetworkResponse<[PlacePlainObject]>) in
            switch response {
            case .success(let items):
                let restaurants = items.map { $0.restaurant }
                self?.output?.didReceiveRestaurants(restaurants)
            case .error:
                self?.output?.didReceiveError()
            }
        }
    }
    
    func startObservingUserLocation() {
       userLocationService?.startObserving()
    }
    
    func stopObservingUserLocation() {
        userLocationService?.stopObserving()
    }
    
    var geocoder = CLGeocoder()
    
    func reverseGeocodeLocation(_ coordinate: CLLocationCoordinate2D) {
        geocoder.cancelGeocode()
        geocoder.reverseGeocodeLocation(coordinate.location) { [weak self] placemarks, _ in
            if let placemark = placemarks?.first {
                self?.output?.didReceiveGeocodePlacemark(placemark)
            }
        }
    }
}

extension RestaurantsListInteractor: UserLocationServiceDelegate {
    func userLocationDidChange(_ coordinate: CLLocationCoordinate2D?) {
        output?.userLocationDidChange(coordinate)
    }
}
