//
//  RestaurantsListViewInput.swift
//  Yandex_test
//
//  Created by Nikita on 20/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

protocol RestaurantsListViewInput: class {
    func configure(with cellObjects: [RestaurantCellObject])
    func showError()
    func setCurrentLocationTitle(_ title: String)
}
