//
//  RestaurantsListDisplayManager.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class RestaurantsListDisplayManager: NSObject, UICollectionViewDataSource, RestaurantsListDisplayManagerInput {    
    var cellObjects: [RestaurantCellObject] = []
    
    func configure(with cellObjects: [RestaurantCellObject]) {
        self.cellObjects = cellObjects
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cellObjects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: RestaurantCell = collectionView.dequeuReusableCell(for: indexPath)
        
        let cellObject = cellObjects[indexPath.item]
        cell.configure(with: cellObject)
        
        return cell
    }
}
