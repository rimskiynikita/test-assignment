//
//  RestaurantsListViewOutput.swift
//  Yandex_test
//
//  Created by Nikita on 20/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

protocol RestaurantsListViewOutput: class {
    func didTriggerViewDidAppear()
    func didTriggerViewWillDisappear()
    
    func didTapSettingsButton()
    func didTapRetry()
}
