//
//  RestaurantsListView.swift
//  Yandex_test
//
//  Created by Nikita on 20/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class RestaurantsListView: UIViewController, RestaurantsListViewInput {
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var settingsButton: UIButton!
    @IBOutlet var locationView: CurrentLocationView!
    
    var dataDisplayManager: (RestaurantsListDisplayManager & UICollectionViewDataSource)?
    var output: RestaurantsListViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCollectionView()
        configureFilterButton()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        configureLocationView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        output?.didTriggerViewDidAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        output?.didTriggerViewWillDisappear()
    }
    
    lazy var collectionViewLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let width = view.bounds.width
        layout.itemSize = CGSize(width: width, height: 100)
        return layout
    }()
    
    func configureCollectionView() {
        collectionView.register(RestaurantCell.self)
        collectionView.dataSource = dataDisplayManager
        
        collectionView.contentInset.top = 84
        collectionView.collectionViewLayout = collectionViewLayout
    }
    
    func configureFilterButton() {
        settingsButton.setTitle(nil, for: .normal)
        settingsButton.setImage(#imageLiteral(resourceName: "filter"), for: .normal)
        settingsButton.tintColor = Colors.blue
        settingsButton.alpha = 0.9
        settingsButton.backgroundColor = .white
        settingsButton.rounded(22)
        settingsButton.dropShadow(color: Colors.lightGray, offset:
            CGSize(width: 0, height: 2))
    }
    
    func configureLocationView() {
        locationView.rounded(22)
        locationView.backgroundColor = Colors.blue
        locationView.dropShadow(color: Colors.blue, radius: 5, opacity: 0.5)
    }
    
    @IBAction func onTapSettings(_ sender: UIButton) {
        output?.didTapSettingsButton()
    }
    
    func configure(with cellObjects: [RestaurantCellObject]) {
        dataDisplayManager?.configure(with: cellObjects)
        collectionView.reloadData()
    }
    
    func setCurrentLocationTitle(_ title: String) {
        locationView.titleLabel.text = title
    }
    
    func showError() {
        let alert = UIAlertController(title: "Что-то пошло не так.", message:
            "Попробуйте повторить запрос.", preferredStyle: .alert)
        
        let retryAction = UIAlertAction(title: "Повторить", style: .default) { [weak self] _ in
            self?.output?.didTapRetry()
        }
        alert.addAction(retryAction)
        
        let closeAction = UIAlertAction(title: "Закрыть", style: .cancel, handler: nil)
        alert.addAction(closeAction)
        
        present(alert, animated: true, completion: nil)
    }
}
