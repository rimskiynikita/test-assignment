//
//  RestaurantCellObject.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

struct RestaurantCellObject {
    let title: String
    let description: String
    let imageRatio: Double
    let imageURL: ((_ width: Int, _ height: Int) -> URL?)
}
