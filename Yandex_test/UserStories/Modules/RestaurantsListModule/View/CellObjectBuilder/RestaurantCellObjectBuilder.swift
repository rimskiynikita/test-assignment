//
//  RestaurantCellObjectBuilder.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Foundation

struct RestaurantCellObjectBuilder {
    func cellObjects(for restaurants: [RestaurantPlainObject]) -> [RestaurantCellObject] {
        var cellObjects = [RestaurantCellObject]()
        restaurants.forEach { restaurant in
            let imageURL: ((_ width: Int, _ height: Int) -> URL?) = { width, height in
                let stringURL = restaurant.picture.url(width: width, height: height)
                return URL(string: stringURL)
            }
            let cellObject = RestaurantCellObject(
                title: restaurant.name,
                description: restaurant.description,
                imageRatio: restaurant.picture.ratio,
                imageURL: imageURL)
            cellObjects.append(cellObject)
        }
        return cellObjects
    }
}
