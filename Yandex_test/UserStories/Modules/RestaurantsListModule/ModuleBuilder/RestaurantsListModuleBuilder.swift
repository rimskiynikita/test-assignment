//
//  RestaurantsListModuleBuilder.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

struct RestaurantsListModuleBuilder {
    func create() -> UIViewController {
        let view: RestaurantsListView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController()
        
        let dataDisplayManager = RestaurantsListDisplayManager()
        view.dataDisplayManager = dataDisplayManager
        
        let presenter = RestaurantsListPresenter()
        view.output = presenter
        presenter.view = view
        
        let router = RestaurantsListRouter()
        presenter.router = router
        
        let interactor = RestaurantsListInteractor()
        presenter.interactor = interactor
        interactor.output = presenter
        
        let networkService = NetworkService()
        interactor.networkService = networkService
        
        let userLocationService = UserLocationService()
        interactor.userLocationService = userLocationService
        userLocationService.delegate = interactor
        
        return view
    }
}
