//
//  SettingsViewInput.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

protocol SettingsViewInput: class {
    func setImageManagerSegmentedControlSegments(_ segments: [(name: String, index: Int)])
    func setImageManagerSegmentedControlIndex(_ index: Int)
    func showSuccessAlert()
}
