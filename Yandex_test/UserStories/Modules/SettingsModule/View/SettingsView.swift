//
//  SettingsView.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class SettingsView: UIViewController, SettingsViewInput {
    
    @IBOutlet var closeButton: UIButton!
    @IBOutlet var imageManagerTitleLabel: UILabel!
    @IBOutlet var imageManagerSegmentedControl: UISegmentedControl!
    @IBOutlet var resetCacheButton: UIButton!
    
    var output: SettingsViewOutput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureCloseButton()
        configureTitleLabel()
        configureImageManagerSegmentedControl()
        configureResetCacheButton()
        
        output?.viewIsReady()
    }
    
    func configureCloseButton() {
        closeButton.setTitle(nil, for: .normal)
        closeButton.setImage(#imageLiteral(resourceName: "close"), for: .normal)
        closeButton.tintColor = Colors.blue
        closeButton.backgroundColor = .white
    }
    
    func configureTitleLabel() {
        imageManagerTitleLabel.numberOfLines = 0
        imageManagerTitleLabel.font = UIFont.systemFont(ofSize: 16)
        imageManagerTitleLabel.textColor = Colors.gray
    }
    
    func configureImageManagerSegmentedControl() {
        imageManagerSegmentedControl.tintColor = Colors.blue
    }
    
    func configureResetCacheButton() {
        resetCacheButton.rounded(10)
        resetCacheButton.layer.borderWidth = 1
        resetCacheButton.layer.borderColor = Colors.blue.cgColor
        
        resetCacheButton.setTitleColor(Colors.blue, for: .normal)
        resetCacheButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)
    }
    
    @IBAction func onTapClose(_ sender: UIButton) {
        output?.didTapClose()
    }
    
    @IBAction func imageManagerSegmentedControlIndexChanged(_ sender: UISegmentedControl) {
        output?.imageManagerSegmentedControlIndexChanged(sender.selectedSegmentIndex)
    }
    
    @IBAction func onTapResetCache(_ sender: UIButton) {
        output?.didTapClearCache()
    }
    
    func setImageManagerSegmentedControlSegments(_ segments: [(name: String, index: Int)]) {
        imageManagerSegmentedControl.removeAllSegments()
        segments.forEach {
            imageManagerSegmentedControl.insertSegment(withTitle:
                $0.name, at: $0.index, animated: false)
        }
    }
    
    func setImageManagerSegmentedControlIndex(_ index: Int) {
        imageManagerSegmentedControl.selectedSegmentIndex = index
    }
    
    func showSuccessAlert() {
        let alert = UIAlertController(title: "Отлично!", message:
            "Кэш успешно очищен.", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "ОК", style: .default, handler: nil)
        alert.addAction(action)
        
        present(alert, animated: true, completion: nil)
    }
}
