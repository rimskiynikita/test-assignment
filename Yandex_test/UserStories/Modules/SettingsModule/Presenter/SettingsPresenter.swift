//
//  SettingsPresenter.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

class SettingsPresenter: SettingsViewOutput {
    weak var view: SettingsViewInput?
    var router: SettingsRouterInput?
    
    let imageManagerDecorator = ImageManagerDecorator.shared
    
    func viewIsReady() {
        let segments = ImageManagerImplementation.implementations.map { ($0.name, $0.rawValue) }
        view?.setImageManagerSegmentedControlSegments(segments)
        
        let currentImageManager = imageManagerDecorator.imageDownloaderImplementation
        view?.setImageManagerSegmentedControlIndex(currentImageManager.rawValue)
    }
    
    func imageManagerSegmentedControlIndexChanged(_ index: Int) {
        let imageManager = ImageManagerImplementation(rawValue: index)
        imageManagerDecorator.imageDownloaderImplementation = imageManager ?? .SDWebImage
    }
    
    func didTapClearCache() {
        imageManagerDecorator.clearCache()
        view?.showSuccessAlert()
    }
    
    func didTapClose() {
        router?.closeModule()
    }
}
