//
//  SettingsModuleBuilder.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

struct SettingsModuleBuilder {
    func create() -> UIViewController {
        let view: SettingsView = UIStoryboard(name: "Main", bundle: nil).instantiateViewController()
        
        let presenter = SettingsPresenter()
        view.output = presenter
        presenter.view = view
        
        let router = SettingsRouter()
        presenter.router = router
        
        return view
    }
}
