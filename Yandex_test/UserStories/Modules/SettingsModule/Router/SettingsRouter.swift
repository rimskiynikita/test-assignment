//
//  SettingsRouter.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

class SettingsRouter: SettingsRouterInput {
    func closeModule() {
        topViewController?.dismiss(animated: true, completion: nil)
    }
}
