//
//  AppDelegate.swift
//  Yandex_test
//
//  Created by Nikita on 20/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let restaurantsListModuleBuilder = RestaurantsListModuleBuilder()
        let restaurantsListModule = restaurantsListModuleBuilder.create()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = restaurantsListModule
        window?.makeKeyAndVisible()
        
        return true
    }
}

