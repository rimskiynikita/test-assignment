//
//  UserLocationServiceDelegate.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import CoreLocation

protocol UserLocationServiceDelegate: class {
    func userLocationDidChange(_ coordinate: CLLocationCoordinate2D?)
}
