//
//  UserLocationServiceInput.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

protocol UserLocationServiceInput: class {    
    func startObserving()
    func stopObserving()
}
