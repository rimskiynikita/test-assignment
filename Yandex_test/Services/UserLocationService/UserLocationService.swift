//
//  UserLocationService.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import CoreLocation

class UserLocationService: NSObject, CLLocationManagerDelegate, UserLocationServiceInput {
    private var locationManager: CLLocationManager?
    
    weak var delegate: UserLocationServiceDelegate?
    
    let desiredAccuracy: CLLocationAccuracy
    
    init(desiredAccuracy: CLLocationAccuracy = kCLLocationAccuracyBest) {
        self.desiredAccuracy = desiredAccuracy
        super.init()
    }
    
    func startObserving() {
        if locationManager == nil {
            locationManager = CLLocationManager()
            locationManager?.desiredAccuracy = desiredAccuracy
            locationManager?.requestAlwaysAuthorization()
            locationManager?.requestWhenInUseAuthorization()
        }
        
        locationManager?.delegate = self
    }
    
    func stopObserving() {
        locationManager?.delegate = nil
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            manager.startUpdatingHeading()
            manager.startUpdatingLocation()
        default:
            manager.requestWhenInUseAuthorization()
        }        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate?.userLocationDidChange(locations.last?.coordinate)
    }
}
