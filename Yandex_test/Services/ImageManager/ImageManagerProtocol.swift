//
//  ImageDownloaderProtocol.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

typealias ImageLoadHandler = ((_ image: UIImage?, _ cached: Bool, _ error: Error?) -> Void)

protocol ImageManagerProtocol {
    func loadImage(with url: URL, completion: @escaping ImageLoadHandler) -> ImageLoadOperationWrapper?
    func clearCache()
}
