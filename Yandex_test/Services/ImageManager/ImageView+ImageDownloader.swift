//
//  ImageView+ImageDownloader.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

extension UIImageView {
    private struct AssociatedKeys {
        static var activeLoadOperation: Void?
    }
    
    private var activeLoadOperation: ImageLoadOperationWrapper? {
        get {
            return objc_getAssociatedObject(self, &AssociatedKeys.activeLoadOperation)
                as? ImageLoadOperationWrapper
        }
        set {
            objc_setAssociatedObject(self, &AssociatedKeys.activeLoadOperation,
                                     newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    private var imageDownloader: ImageManagerProtocol {
        return ImageManagerDecorator.shared
    }
    
    func setImage(with url: URL?, animated: Bool = true) {
        guard let url = url else { return }
        
        cancelCurrentImageLoad()
        activeLoadOperation = imageDownloader.loadImage(with: url) { [weak self] image, cached, _ in
            self?.activeLoadOperation = nil
            
            guard let `self` = self else { return }
            UIView.transition(with: self, duration: animated && !cached ? 0.3 : 0, options:
                .transitionCrossDissolve, animations: {
                    self.image = image
            }, completion: nil)
        }
    }
    
    func cancelCurrentImageLoad() {
        activeLoadOperation?.cancel()
    }
}

