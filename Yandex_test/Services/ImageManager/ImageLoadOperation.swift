//
//  ImageLoadOperation.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import SDWebImage

protocol ImageLoadOperation {
    func cancel()
}

/* Need to implement wrapper due to private
 access to SDWebImageOperation instance */

struct ImageLoadOperationWrapper {
    let SDWebImageOperation: SDWebImageOperation?
    let imageManagerOperation: ImageLoadOperation?
    
    init(SDWebImageOperation: SDWebImageOperation? = nil,
         imageManagerOperation: ImageLoadOperation? = nil) {
        
        self.SDWebImageOperation = SDWebImageOperation
        self.imageManagerOperation = imageManagerOperation
    }
    
    func cancel() {
        if let operation = SDWebImageOperation {
            operation.cancel()
        }
        imageManagerOperation?.cancel()
    }
}
