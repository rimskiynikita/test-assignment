//
//  ImageDownloaderDecorator.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import UIKit

class ImageManagerDecorator: ImageManagerProtocol {
    static let shared = ImageManagerDecorator()
    
    var imageDownloaderImplementation: ImageManagerImplementation {
        didSet {
            imageDownloader = imageDownloaderImplementation.imageDownloader
        }
    }
    var imageDownloader: ImageManagerProtocol
    
    init(_ implementation: ImageManagerImplementation = .SDWebImage) {
        self.imageDownloaderImplementation = implementation
        self.imageDownloader = implementation.imageDownloader
    }
    
    func loadImage(with url: URL, completion: @escaping ImageLoadHandler) -> ImageLoadOperationWrapper? {
        return imageDownloader.loadImage(with: url, completion: completion)
    }
    
    func clearCache() {
        return imageDownloader.clearCache()
    }
}
