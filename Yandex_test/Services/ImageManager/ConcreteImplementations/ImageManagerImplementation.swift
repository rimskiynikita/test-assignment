//
//  ImageDownloaderImplementation.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

enum ImageManagerImplementation: Int {
    case SDWebImage = 0
    case Kingfisher
    case Nuke
    
    var imageDownloader: ImageManagerProtocol {
        switch self {
        case .SDWebImage:
            return SDWebImageImageManager()
        case .Kingfisher:
            return KingfisherImageManager()
        case .Nuke:
            return NukeImageManager()
        }
    }
    
    var name: String {
        switch self {
        case .SDWebImage:
            return "SDWebImage"
        case .Kingfisher:
            return "Kingfisher"
        case .Nuke:
            return "Nuke"
        }
    }
    
    static var implementations: [ImageManagerImplementation] {
        var values: [ImageManagerImplementation] = []
        var index = 0
        while let element = self.init(rawValue: index) {
            values.append(element)
            index += 1
        }
        return values
    }
}
