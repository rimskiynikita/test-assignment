//
//  NukeImageDownloader.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Nuke

extension ImageTask: ImageLoadOperation { }

class NukeImageManager: ImageManagerProtocol {
    let imageDownloader: ImagePipeline
    let imageCache: ImageCache
    
    init(_ imageDownloader: ImagePipeline = .shared, _ imageCache: ImageCache = .shared) {
        self.imageDownloader = imageDownloader
        self.imageCache = imageCache
    }
    
    func loadImage(with url: URL, completion: @escaping ImageLoadHandler) -> ImageLoadOperationWrapper? {
        if let response = imageCache.cachedResponse(for: .init(url: url)) {
            completion(response.image, true, nil)
            return nil
        } 
        let operation = imageDownloader.loadImage(with: url) { response, error in
            completion(response?.image, false, error)
        }
        return ImageLoadOperationWrapper(imageManagerOperation: operation)
    }
    
    func clearCache() {
        imageCache.removeAll()
    }
}
