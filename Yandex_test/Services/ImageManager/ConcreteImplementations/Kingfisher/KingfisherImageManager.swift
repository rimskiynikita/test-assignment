//
//  KingfisherImageDownloader.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Kingfisher

extension RetrieveImageTask: ImageLoadOperation { }

class KingfisherImageManager: ImageManagerProtocol {
    let imageManager: KingfisherManager
    
    init(_ imageManager: KingfisherManager = .shared) {
        self.imageManager = imageManager
    }
    
    func loadImage(with url: URL, completion: @escaping ImageLoadHandler) -> ImageLoadOperationWrapper? {
        let resource = ImageResource(downloadURL: url)
        let operation = imageManager.retrieveImage(with: resource, options:
        nil, progressBlock: nil) { image, error, cacheType, _ in
            completion(image, cacheType != .none, error)
        }
        return ImageLoadOperationWrapper(imageManagerOperation: operation)
    }
    
    func clearCache() {
        imageManager.cache.clearMemoryCache()
        imageManager.cache.clearDiskCache()
    }
}
