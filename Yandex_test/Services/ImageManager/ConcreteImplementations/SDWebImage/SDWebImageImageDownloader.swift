//
//  SDWebImageImageDownloader.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import SDWebImage

class SDWebImageImageManager: ImageManagerProtocol {
    let imageManager: SDWebImageManager
    
    init(_ imageManager: SDWebImageManager = .shared()) {
        self.imageManager = imageManager
    }
    
    func loadImage(with url: URL, completion: @escaping ImageLoadHandler) -> ImageLoadOperationWrapper? {
        let operation = imageManager.loadImage(with: url, options:
        [], progress: nil) { image, _, error, cacheType, _, _ in
            completion(image, cacheType != .none, error)
        }
        
        return ImageLoadOperationWrapper(SDWebImageOperation: operation)
    }
    
    func clearCache() {
        imageManager.imageCache?.clearMemory()
        imageManager.imageCache?.clearDisk(onCompletion: nil)
    }
}
