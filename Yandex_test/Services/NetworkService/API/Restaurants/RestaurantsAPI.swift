//
//  RestaurantsAPI.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Moya

enum RestaurantsAPI {
    case catalogue(latitude: Double, longitude: Double)
}

extension RestaurantsAPI: TargetType {
    var baseURL: URL {
        let stringURL = AppConstants.baseURL + "/api/v2"
        guard let url = URL(string: stringURL) else { fatalError("Invalid base URL.") }
        return url
    }
    
    var path: String {
        switch self {
        case .catalogue:
            return "/catalog"
        }
    }
    
    var method: Method {
        return .get
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var parameters: [String: Any] {
        switch self {
        case .catalogue(let latitude, let longitude):
            return [
                "latitude" : latitude,
                "longitude" : longitude
            ]
        }
    }
    
    var task: Task {
        return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
    }
    
    var headers: [String : String]? {
        return ["Content-type" : "application/json"]
    }
}
