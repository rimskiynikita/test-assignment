//
//  NetworkService.swift
//  Yandex_test
//
//  Created by Nikita on 22/09/2018.
//  Copyright © 2018 Nikita. All rights reserved.
//

import Moya

enum NetworkResponse<T: Decodable> {
    case success(T)
    case error
}

protocol NetworkServiceProtocol {
    func fetchObjects<T, API: TargetType>(_ target: API, keyPath:
        String?, completion: @escaping (NetworkResponse<T>) -> Void)
}

struct NetworkService: NetworkServiceProtocol {
    func fetchObjects<T, API>(_ target: API, keyPath: String?, completion:
        @escaping (NetworkResponse<T>) -> Void) where T : Decodable, API : TargetType {
        
        MoyaProvider<API>().request(target) { result in
            switch result {
            case .success(let response):
                do {
                    let results = try response.map(T.self, atKeyPath: keyPath)
                    completion(NetworkResponse.success(results))
                } catch {
                    completion(NetworkResponse.error)
                }
            case .failure:
                completion(NetworkResponse.error)
            }
        }
    }
}
